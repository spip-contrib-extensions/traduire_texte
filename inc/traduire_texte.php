<?php

/**
 * Retourne un traducteur disponible
 * @return \TT_Traducteur|false
 */
function TT_traducteur(){
	static $traducteur = null;
	if (is_null($traducteur)){
		include_spip('inc/config');

		$traducteur = false;

		$traducteurs_dispo = [
			'bing' => '_BING_APIKEY',
			'deepl' => '_DEEPL_APIKEY',
			'google' => '_GOOGLETRANSLATE_APIKEY',
			'yandex' => '_YANDEX_APIKEY',
		];
		$classes = [
			'bing' => 'TT_Traducteur_Bing',
			'deepl' => 'TT_Traducteur_DeepL',
			'google' => 'TT_Traducteur_GGTranslate',
			'yandex' => 'TT_Traducteur_Yandex',
		];

		foreach ($traducteurs_dispo as $traducteur_dispo => $nom_constante){
			if ((defined($nom_constante) and $key = constant($nom_constante))
				or $key = lire_config('traduiretexte/cle_' . $traducteur_dispo)){
				$class = $classes[$traducteur_dispo];
				if (!class_exists($class)) {
					include_spip('inc/traducteurs');
				}
				$traducteur = new $class($key);
				break;
			}
		}

		if (!$traducteur and defined('_TRANSLATESHELL_CMD')){
			include_spip('inc/traducteurs');
			$traducteur = new TT_Traducteur_Shell();
		}
	}
	return $traducteur;
}


/**
 * Découpe un code HTML en paragraphes.
 *
 * Découpe un texte en autant de morceaux que de balises `<p>`.
 * Cependant, si la longueur du paragraphe dépasse `$maxlen` caractères,
 * il est aussi découpé.
 * @param string $texte
 *     Texte à découper
 * @param int $maxlen
 *     Nombre maximum de caractères.
 * @param bool $html
 *     True pour conserver les balises HTML ; false pour les enlever.
 * @return array
 *     Couples [[hash => '..',  'texte' => 'paragraphe'],...]
 */
function TT_decouper_texte($texte, $maxlen = 0, $html = true){
	$liste = array();
	$texte = trim($texte);

	if (strlen($texte)==0){
		return $liste;
	}

	$prep = html2unicode($texte);
	// si c'est du texte html, on split sur les <p>, sinon sur les doubles saut de ligne
	if (strpos($prep,'</p>') !== false) {
		$prep = preg_split(",(<p\b[^>]*>),i", $prep, -1, PREG_SPLIT_DELIM_CAPTURE);
	}
	else {
		if (strpos($texte, "\r") !== false){
			$prep = str_replace("\r\n", "\n", $prep);
			$prep = str_replace("\r", "\n", $prep);
		}
		$prep = preg_split(",(\n\n+),i", $prep, -1, PREG_SPLIT_DELIM_CAPTURE);
	}
	// 0, 2, 4, 6... : le texte
	// 1, 3, 5, 7... : les separateurs <p..>

	for ($i=0;$i<count($prep);$i+=2) {

		// remettre le <p...> en debut de ligne si besoin
		$line = (($html and $i>0) ? $prep[$i-1] : '') . $prep[$i];

		if (strlen($line)) {
			if (!$html){
				$line = preg_replace(",<[^>]*>,i", " ", $line);
			}

			if ($maxlen){
				$a = array();
				// max line = XXX chars
				while (mb_strlen($line)>$maxlen){
					$len = intval($maxlen*0.6); // 60% de la longueur
					$debut = mb_substr($line, 0, $len);
					$suite = mb_substr($line, $len);
					$point = mb_strpos($suite, '.');

					// chercher une fin de phrase pas trop loin
					// ou a defaut, une virgule ; au pire un espace
					if ($point===false){
						$point = mb_strpos(preg_replace('/[,;?:!]/', ' ', $suite), ' ');
					}
					if ($point===false){
						$point = mb_strpos($suite, ' ');
					}
					if ($point===false){
						$point = 0;
					}
					$a[] = $debut . mb_substr($suite, 0, 1+$point);
					$line = mb_substr($line, $len+1+$point);
				}
				foreach ($a as $l){
					$liste[] = array('hash' => TT_hash($l), 'legacy_hash' => md5($l), 'texte' => $l);
				}
			}

			$liste[] = array('hash' => TT_hash($line),'legacy_hash' => md5($line), 'texte' => $line);
		}
	}

	// mettre a jour en base les anciens hash au passage
	TT_update_legacy_hash($liste);
	return $liste;
}

/**
 * Hash d'un texte
 * @param $texte
 * @return string
 */
function TT_hash($texte) {
	return md5(TT_normaliser_texte($texte));
}

/**
 * normaliser le texte, en nettoyant les espace avant/apres et en standardisant les retours lignes
 * permet de conserver un meme hash même en cas de petites variations de forme dans le texte source
 * @param $texte
 * @return string
 */
function TT_normaliser_texte($texte) {
	$texte = trim($texte);
	if (strpos($texte, "\r") !== false) {
		$texte = str_replace("\r\n", "\n", $texte);
		$texte = str_replace("\r", "\n", $texte);
	}

	return $texte;
}

/**
 * Reintroduire sur la traduction les espaces en debut et en fin de texte source
 * @param string $texte_source
 * @param string $traduction
 * @return string
 */
function TT_espacer_traduction($texte_source, $traduction) {
	$len = strlen($texte_source);
	if (preg_match(",^\s+,", $texte_source, $m)) {
		$traduction = $m[0] . $traduction;
	}
	if (($len_signif = strlen(rtrim($texte_source))) !== $len) {
		$traduction .= substr($texte_source, $len_signif);
	}
	return $traduction;
}

/**
 * Convertir a la volee les anciens hash non trimes si il en reste
 * @param $parts
 */
function TT_update_legacy_hash($parts) {
	$legacies_hash = sql_allfetsel( 'DISTINCT hash', "spip_traductions", sql_in('hash', array_column($parts, 'legacy_hash')));
	if (count($legacies_hash)) {
		$legacies_hash = array_column($legacies_hash, 'hash');
		$hash_convert = array_column($parts, 'hash', 'legacy_hash');
		foreach ($legacies_hash as $legacy_hash) {
			$new_hash = $hash_convert[$legacy_hash];
			if ($new_hash !== $legacy_hash) {
				$trads = sql_allfetsel("*", "spip_traductions", "hash=" . sql_quote($legacy_hash));
				foreach ($trads as $trad) {
					$set = [
						'hash' => $new_hash,
						'texte' => TT_normaliser_texte($trad['texte']),
					];
					sql_updateq("spip_traductions", $set, "hash=" . sql_quote($trad['hash']) . " AND langue=".sql_quote($trad['langue']));
					spip_log("[langue ".$trad['langue']." / legacy hash $legacy_hash] => $new_hash", 'translate' . _LOG_DEBUG);
				}
			}
		}
	}
}

/**
 * Traduire sans utiliser le cache ni mettre en cache le resultat
 *
 * Retourne le texte traduit encadré d’une div indiquant la langue et sa direction.
 *
 * @param string $texte
 * @param string $destLang
 * @param string $srcLang
 * @param bool $raw
 * @return string|false
 */
function traduire_texte($texte, $destLang = 'fr', $srcLang = 'en'){
	if (strlen(trim($texte))==0){
		return '';
	}

	//$text = rawurlencode( $text );
	$destLang = urlencode($destLang);
	$srcLang = urlencode($srcLang);

	$traducteur = TT_traducteur();
	if (!$traducteur){
		return false;
	}

	$trans = $traducteur->traduire($texte, $destLang, $srcLang);

	if (strlen($trans)){
		$ltr = lang_dir($destLang, 'ltr', 'rtl');
		return "<div dir='$ltr' lang='$destLang'>$trans</div>";
	} else {
		return false;
	}
}


/**
 * Traduire avec un cache
 *
 * Le texte est découpé en paragraphe ; chaque paragraphe est traduit et mis en cache.
 * Si un paragraphe dépasse la taille maximale acceptée par le traducteur, il sera découpé
 * lui aussi en morceaux.
 *
 * @param string $texte
 * @param string $destLang
 * @param string $srcLang
 * @param array $options {
 *   @var bool $raw
 *         Retourne un tableau des blocs [ [hash, source, trad, new(bool)],... ]
 *   @var bool $throw
 *         Lancer une exception en cas d'erreur
 *   @var bool $mode_test
 *         pour tester le traducteur : on ne cherche pas les traductions deja connues et on ne stocke pas en cache a la fin
 * }
 * @return string|false|array
 * @throws Exception
 */
function traduire($texte, $destLang = 'fr', $srcLang = 'en', $options = array()){
	if (strlen(trim($texte))==0){
		return '';
	}
	$throw = ((isset($options['throw']) and $options['throw']) ? true : false);

	$traducteur = TT_traducteur();
	if (!$traducteur){
		if ($throw) {
			throw new \Exception(_T('traduiretexte:erreur_aucun_traducteur_disponible'));
		}
		return false;
	}

	$parts = TT_decouper_texte($texte, $traducteur->maxlen, true);

	$deja_traduits = array();
	if (empty($options['mode_test'])) {
		$deja_traduits = sql_allfetsel(
			array('hash', 'texte'),
			"spip_traductions",
			array(
				sql_in('hash', array_column($parts, 'hash')),
				'langue = ' . sql_quote($destLang),
			)
		);
	}

	if ($deja_traduits){
		$deja_traduits = array_column($deja_traduits, 'texte', 'hash');
	}

	$inserts = array();
	$fail = false;
	// on passe en deux fois pour envoyer tous les morceaux a traduire en un coup
	// certains traducteurs etant capables de traiter un tableau de texte
	$todo = [];
	foreach ($parts as $k=>$part){
		$hash = $part['hash'];
		if (!isset($deja_traduits[$hash])){
			$todo[$hash] = trim($part['texte']);
		}
	}

	if ($todo) {
		$trads = $traducteur->traduire($todo, $destLang, $srcLang, $throw);
		if (count($trads) !== count($todo)) {
			spip_log('[' . $destLang . "] ECHEC traduction tableau : " . count($trads) ." vs ". count($todo)." attendus", 'translate' . _LOG_ERREUR);
			return "";
		}
		$trads = array_combine(array_keys($todo), array_values($trads));
		foreach ($trads as $hash => $trad) {
			if ($trad !== false) {
				$trad = TT_normaliser_texte($trad);
				$deja_traduits[$hash] = $trad;
				$inserts[] = array(
					"hash" => $hash,
					"texte" => $trad,
					"langue" => $destLang
				);
			}
			else {
				$fail = true;
			}
		}
		unset($todo);
		unset($trads);

		// fail ou pas, on mets en cache les traductions faites
		// sauf si on est en mode_test
		if ($inserts and empty($options['mode_test'])){
			sql_insertq_multi("spip_traductions", $inserts);
		}

		if ($fail) {
			return "";
		}
	}

	foreach ($parts as $k=>$part){
		$hash = $part['hash'];
		if (!isset($deja_traduits[$hash])) {
			// NE devrait jamais arriver !
			spip_log('[' . $destLang . "] ECHEC traduction parts : il manque le hash $hash a la fin", 'translate' . _LOG_CRITIQUE);
			return "";
		}
		// espacer les traductions conformement au texte source
		$parts[$k]['trad'] = TT_espacer_traduction($part['texte'], $deja_traduits[$hash]);
	}
	unset($deja_traduits);


	// retour brut
	if (!empty($options['raw'])){
		$new_hashes = array_column($inserts, 'hash');
		$res = array();
		while(count($parts)) {
			$part = array_shift($parts);
			$res[] = array(
				'hash' => $part['hash'],
				'source' => $part['texte'],
				'trad' => $part['trad'],
				'new' => in_array($part['hash'], $new_hashes),
			);
		}
		return $res;
	}

	$traductions = array_column($parts, 'trad');
	unset($parts);

	$traductions = implode("", $traductions);
	if (!empty($options['format']) and $options['format']=='text'){
		return $traductions;
	}

	$ltr = lang_dir($destLang, 'ltr', 'rtl');
	return "<div dir='$ltr' lang='$destLang'>$traductions</div>";
}
