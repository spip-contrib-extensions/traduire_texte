<?php


/** Description de l’outil de traduction */
abstract class TT_Traducteur {
	/** @var string Nom de l’outil */
	public $type;
	/** @var string Clé d’api */
	public $apikey;
	/** @var int Maximum de caractères traitables en un coup */
	public $maxlen;
	/** @var bool Est-ce que le traducteur peut traiter un tableau de textes ou non ? */
	public $isArrayCapable;

	/**
	 * TT_Traducteur constructor.
	 * @param string $apikey
	 */
	public function __construct($apikey = null){
		$this->apikey = $apikey;
	}

	/**
	 * @param string|string[] $texte
	 * @param string $destLang
	 * @param string $srcLang
	 * @param bool $throw
	 * @return string|array
	 * @throws Exception
	 */
	public function traduire($texte, $destLang = 'fr', $srcLang = 'en', $throw = false){
		$log = 'Trad:' . $this->type;
		$todo = [];
		if (is_array($texte)) {
			// si le traducteur ne sait pas faire ou que ca ne sera pas plus rapide, on itere sur le tableau
			if (!$this->isArrayCapable or count($texte) == 1) {
				$ress = [];
				$erreur = false;
				foreach ($texte as $k=>$t) {
					if (!$erreur){
						$res = $this->traduire($t, $destLang, $srcLang, $throw);
						if ($res === false) {
							$erreur = true;
						}
					}
					$ress[$k] = $res;
				}
				return $ress;
			}

			$len = array_sum(array_map('mb_strlen', $texte));
			while ($len > $this->maxlen and count($texte) > 1) {
				$t = array_pop($texte);
				$len -= mb_strlen($t);
				array_unshift($todo, $t);
			}
			$c = count($texte);
			$log .= " array($c)";
			$extrait = mb_substr(reset($texte), 0, 40);
		}
		else {
			if (strlen(trim($texte))==0){
				return '';
			}
			$len = mb_strlen($texte);
			$extrait = mb_substr($texte, 0, 40);
		}
		spip_log($log . ' ' . $len . 'c. : ' . $extrait . ($len>40 ? '...' : ''), 'translate' . _LOG_DEBUG);

		$erreur = false;
		$res = $this->_traduire($texte, $destLang, $srcLang, $erreur);
		if ($erreur) {
			spip_log($erreur, 'translate' . _LOG_ERREUR);
			if ($throw) {
				throw new \Exception($erreur);
			}
		}

		if ($todo) {
			$todo = $this->traduire($todo, $destLang, $srcLang, $throw);
			while (count($todo)) {
				$res[] = array_shift($todo);
			}
		}

		return $res;
	}

	abstract protected function _traduire($texte, $destLang, $srcLang, &$erreur);
}

/**
 * Traduire avec Bing
 */
class TT_Traducteur_Bing extends TT_Traducteur {
	public $type = 'bing';
	public $maxlen = 10000;
	public $isArrayCapable = false;

	protected function _traduire($texte, $destLang, $srcLang, &$erreur){
		// Bon sang, si tu n'utilises pas .NET, ce truc est documenté par les corbeaux
		// attaquer le machin en SOAP (la méthode HTTP ne convient que pour des textes très courts (GET, pas POST)
		try {
			$client = new \SoapClient("http://api.microsofttranslator.com/V2/Soap.svc");
			$params = array(
				'appId' => $this->apikey,
				'text' => $texte,
				'from' => $srcLang,
				'to' => $destLang
			);
			$translation = $client->translate($params);
		} catch (Exception $e) {
			$erreur = $e->getMessage();
			return false;
		}

		return $translation->TranslateResult;
	}
}

/**
 * Traduire avec DeepL
 */
class TT_Traducteur_DeepL extends TT_Traducteur {
	public $type = 'deepl';
	public $maxlen = 29000; // The request size should not exceed 30kbytes
	public $isArrayCapable = true;
	protected $apiVersion = 2;

	protected function _traduire($texte, $destLang, $srcLang, &$erreur){

		include_spip('lib/deepl-php-lib/autoload');
		try {
			$deepl   = new BabyMarkt\DeepL\DeepL($this->apikey, $this->apiVersion);
			$tagHandling = [];
			$ignoreTags = [];
			foreach (is_array($texte)?$texte:[$texte] as $t) {
				if (strpos($t, "</") !== false and preg_match(",</\w+>,ms", $t)) {
					$tagHandling = ['xml'];
					// on peut encapsuler dans un <x>..</x> le contenu qu'on veut conserver intact
					$ignoreTags = ['x'];
					break;
				}
			}
			$traduction = $deepl->translate($texte, $srcLang, $destLang, $tagHandling, $ignoreTags);
			if (is_array($texte)) {
				if (count($texte) == 1 and is_string($traduction)) {
					$traduction = array_combine(array_keys($texte), [$traduction]);
				}
				else {
					$traduction = array_combine(array_keys($texte), array_column($traduction, 'text'));
				}
			}
		} catch (Exception $e) {
			$erreur = $e->getMessage();
			return false;
		}

		return $traduction;
	}
}

/**
 * Traduire avec Google Translate
 */
class TT_Traducteur_GGTranslate extends TT_Traducteur {
	public $type = 'google';
	public $maxlen = 4500;
	public $isArrayCapable = false;

	protected function _traduire($texte, $destLang = 'fr', $srcLang = 'en', &$erreur){
		$destLang = urlencode($destLang);
		$srcLang = urlencode($srcLang);

		$url_page = "https://www.googleapis.com/language/translate/v2?";
		$parameters = "key=" . $this->apikey . "&source=$srcLang&target=$destLang&q=" . rawurlencode($texte);

		# $parameters_explode = explode("&", $parameters);
		# $nombre_param = count($parameters_explode);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url_page);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_REFERER, !empty($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : "");
		#curl_setopt($ch, CURLOPT_POST, nombre_param);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-HTTP-Method-Override: GET'));
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$body = curl_exec($ch);
		curl_close($ch);

		$json = json_decode($body, true);

		if (isset($json["error"])){
			spip_log($json, 'translate' . _LOG_DEBUG);
			$erreur = _T('traduiretexte:erreur') . " " . $json["error"]['code'] . ": " . $json["error"]['message'];
			return false;
		}

		return urldecode($json["data"]["translations"][0]["translatedText"]);
	}
}

/**
 * Traduire avec Yandex
 */
class TT_Traducteur_Yandex extends TT_Traducteur {
	public $type = 'yandex';
	public $maxlen = 10000;
	public $isArrayCapable = false;

	protected function _traduire($texte, $destLang = 'fr', $srcLang, &$erreur){
		$destLang = urlencode($destLang);
		//yandex peut deviner la langue source
		if (isset($srcLang)){
			$srcLang = urlencode($srcLang);
			$lang = "$srcLang-$destLang";
		} else {
			$lang = $destLang;
		}

		$url_page = "https://translate.yandex.net/api/v1.5/tr.json/translate?";
		//  & [format=<text format>] & [options=<translation options>] & [callback=<name of the callback function>]
		$parameters = "key=" . $this->apikey . "&text=" . rawurlencode($texte) . "&lang=$lang";

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url_page);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_REFERER, !empty($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : "");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-HTTP-Method-Override: GET'));
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$body = curl_exec($ch);
		curl_close($ch);

		//{"code":200,"lang":"fr-en","text":["hello"]}
		$json = json_decode($body, true);

		if (isset($json['code']) && $json['code']>200){
			spip_log($json, 'translate' . _LOG_DEBUG);
			$erreur = _T('traduiretexte:erreur') . " " . $json['code'] . ": " . $json['message'];
			return false;
		}

		return urldecode($json["text"][0]);
	}
}


class TT_Traducteur_Shell extends TT_Traducteur {
	public $type = 'shell';
	public $maxlen = 1000;
	public $isArrayCapable = false;

	public function _traduire($texte, $destLang = 'fr', $srcLang = 'en', &$erreur){
		if (!defined('_TRANSLATESHELL_CMD')){
			$erreur = "chemin de Translate shell non défini";
			return false;
		}

		return $this->translate_line($texte, $destLang);

		/*
		// Équivalent ~ de l’ancien fonctionnement. (qui supprimait les tags html)
		$liste = TT_decouper_texte($texte, $this->maxlen);
		foreach ($liste as $l) {
			spip_log("IN: " . $l, 'translate');
			$trad = $this->translate_line($l, $destLang);
			spip_log("OUT: " . $trad, 'translate');
			$trans[] = $trad;
		}
		return join(" ", $trans);
		*/
	}

	public function translate_line($texte, $destLang){
		if (strlen(trim($texte))==0){
			return '';
		}
		$descriptorspec = array(
			0 => array("pipe", "r"),
			1 => array("pipe", "w")
		);
		$cmd = _TRANSLATESHELL_CMD . ' -b ' . ':' . escapeshellarg($destLang);
		$cmdr = proc_open($cmd, $descriptorspec, $pipes);
		if (is_resource($cmdr)){
			fwrite($pipes[0], $texte) && fclose($pipes[0]);
			$trad = stream_get_contents($pipes[1]);
			fclose($pipes[1]);
		}
		return $trad;
	}

}