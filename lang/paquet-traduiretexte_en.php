<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// I
	'traduiretexte_description' => 'Automatic translation of content using BING, Google Translate or Yandex APIs
		
	Requires an API identification key.',
	'traduiretexte_nom' => 'Translate text',
	'traduiretexte_slogan' => 'Automatic translation of content',
);

?>