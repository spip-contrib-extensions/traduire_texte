<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_traduire' => 'Traduire',
	'bouton_supprimer_cette_traduction' => 'Supprimer cette traduction',

	// C
	'cfg_titre_parametrages' => 'Paramétrage du plugin',
	'configuration_traduire_texte' => 'Configurer Traduire texte',

	// E
	'erreur' => 'Erreur',
	'erreur_inconnue_traduire' => 'Une erreur inconnue est survenue pendant le calcul de la traduction',
	'erreur_traduire' => 'Une erreur est survenue pour calculer la traduction :',
	'erreur_pas_de_texte' => 'Aucun texte à traduire',
	'erreur_pas_de_langue_source' => 'Choisissez une langue source',
	'erreur_pas_de_langue_cible' => 'Choisissez dans quelle langue traduire',
	'erreur_langues_identiques' => 'Choisissez une langue différente de la langue source',
	'erreur_aucun_traducteur_disponible' => 'Aucun traducteur disponible n\'est configuré',

	// I
	'info_aucune_traduction' => 'Aucune traduction',
	'info_1_traduction' => 'Une traduction',
	'info_nb_traductions' => '@nb@ traductions',

	// L
	'label_cle_bing' => 'Clé d’API pour <b>Bing</b>',
	'label_cle_deepl' => 'Clé d’API pour <b>DeepL</b>',
	'label_cle_google' => 'Clé d’API pour <b>Google Translate</b>',
	'label_cle_yandex' => 'Clé d’API pour <b>Yandex</b>',
	'label_texte_source' => 'Texte source',
	'label_hash' => 'Hash',
	'label_traduction' => 'Traduction',
	'label_langue_source' => 'Langue source',
	'label_langue_traduction' => 'Traduire vers',
	'langue' => 'Langue',

	// S
	'succes_traduction' => 'Traduction effectuée',

	// T
	'tester_traduire_texte' => 'Tester traduire texte',

	// U
	'une_constante_surcharge' => 'Une constante déclare cette clé d’API (elle surcharge cette configuration).',
);

?>